#include <SFML/Graphics.hpp>
#include "Player.h"

int main()
{
	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game window setup
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze Escape", sf::Style::Titlebar | sf::Style::Close);
	gameWindow.setMouseCursorVisible(false);

	// Game Clock
	sf::Clock gameClock;

	// TEMP: REMOVE
	Player test(sf::Vector2f(100.0f, 100.0f));


	// -----------------------------------------------
	// Game Loop
	// -----------------------------------------------
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{

		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				gameWindow.close();
			}


		}
		// Close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}

		test.Input();


		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		sf::Time frameTime = gameClock.restart();
		test.Update(frameTime);


		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		gameWindow.clear();

		// Draw everything
		test.DrawTo(gameWindow);

		gameWindow.display();
	}

	return 0;
}