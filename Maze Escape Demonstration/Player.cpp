#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2f startingPos)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 100, 100, 5)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
{
	sprite.setPosition(startingPos);

	// Set up animation clips
	AddClip("walkDown", 0, 3);
	PlayClip("walkDown");
}


void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
}

void Player::Update(sf::Time frameTime)
{
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	// Move the player to the new position
	sprite.setPosition(newPosition);

	// Call base class update
	AnimatingObject::Update(frameTime);
}