#include "SpriteObject.h"

SpriteObject::SpriteObject(sf::Texture& newTexture)
	: sprite(newTexture)
{
}

void SpriteObject::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}
